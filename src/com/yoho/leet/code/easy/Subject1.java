package com.yoho.leet.code.easy;

import java.util.HashMap;

/**
 * 力扣习题（1）
 *
 * @author yhou
 * @ProjectName leetcode
 * @Description:
 * @date 2021/04/21 3:55 下午
 */
public class Subject1 {


    public static void main(String[] args) {
        //  基础数据
        int[] nums = {3, 2, 4};
        int target = 7;

        // 方法1
        int[] result = numberSum1(nums, target);
        for (int a : result) {
            System.out.println(a);
        }
        System.out.println("====================================================");

        // 方法2
        result = numberSum2(nums, target);
        for (int a : result) {
            System.out.println(a);
        }
    }

    /**
     * 在数组中找出和为目标的两个整数，返回他们的数组下标
     * 暴力破解
     *
     * @param nums   数组
     * @param target 目标值
     */
    public static int[] numberSum1(int[] nums, int target) {
        for (int i = 0; i < nums.length; i++) {
            for (int j = i + 1; j < nums.length; j++) {
                if (nums[i] + nums[j] == target) {
                    return new int[]{i, j};
                }
            }
        }
        return new int[0];

    }


    /**
     * 在数组中找出和为目标的两个整数，返回他们的数组下标
     * hash值方式
     *
     * @param nums   数组
     * @param target 目标值
     */
    public static int[] numberSum2(int[] nums, int target) {
        // map<nums中的值，nums的下标>
        HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
        for (int i = 0; i < nums.length; i++) {
            if(map.containsKey(target-nums[i])){
                return new  int[]{ map.get(target-nums[i]),i};
            }
            map.put(nums[i],i);
        }
        return new int[0];
    }
}
